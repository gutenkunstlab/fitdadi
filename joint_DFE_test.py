#!/usr/bin/env python

# Name of job
#PBS -N cache
# How much CPU time per job?
#PBS -l cput=560:0:0
#PBS -l walltime=20:0:0

#PBS -W group_list=rgutenk
#PBS -V
#PBS -j oe

# For ocelote
#PBS -q oc_high_pri
#PBS -l select=1:ncpus=28:mem=56gb
#PBS -l place=free:shared

import sys,os
if 'PBS_O_WORKDIR' in os.environ:
    # Set module search path so it will search in qsub directory
    sys.path.insert(0, os.environ['PBS_O_WORKDIR'])
    # Set current working directory to qsub directory
    os.chdir(os.environ['PBS_O_WORKDIR'])
# If this is an array job, get process index.
process_ii = int(os.environ.get('PBS_ARRAY_INDEX', 1)) - 1

import cPickle
import numpy as np, matplotlib.pyplot as plt
import dadi
from dadi import Numerics, PhiManip, Integration, Spectrum
import Selection as S1d, Selection_2d as S2d
reload(S2d)

@dadi.Numerics.make_extrap_func
def split_mig_sel(params, ns, pts):
    nu1,nu2,T,m,gamma1,gamma2 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx, gamma=gamma1)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    phi = Integration.two_pops(phi, xx, T, nu1, nu2, m12=m, m21=m,
                               gamma1=gamma1, gamma2=gamma2)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

@dadi.Numerics.make_extrap_func
def equil_sel(params, ns, pts):
    gamma1 = params[0]

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx, gamma=gamma1)

    fs = Spectrum.from_phi(phi, ns, (xx,))
    return fs

def split_mig_single_sel(params, ns, pts):
    nu1,nu2,T,m,gamma = params
    return split_mig_sel((nu1,nu2,T,m,gamma,gamma), ns, pts)

def trivial_fs(params, ns, pts):
    """
    For testing.
    """
    return Spectrum([[1]], mask_corners=False)

demo_params = [1,0.2,0.2,0]
ns = [20,20]
pts_l = [300,400,500]

#s1 = S1d.spectra(demo_params, ns, split_mig_single_sel, pts_l=pts_l, Npts=50,
#                 int_bounds=(1e-4,2000), echo=True, mp=True)
#fid = file('joint_test_1d.bpkl', 'w')
#cPickle.dump(s1, fid, protocol=2)
#fid.close()
#
#s1_single = S1d.spectra([], ns[:1], equil_sel, pts_l=pts_l, Npts=50,
#                        int_bounds=(1e-4,2000), echo=True, mp=True)
#fid = file('joint_test_single_pop_1d.bpkl', 'w')
#cPickle.dump(s1_single, fid, protocol=2)
#fid.close()
#
#s2 = S2d.spectra2d(demo_params, ns, split_mig_sel, pts=pts_l, Npts=50,
#                   int_bounds=(1e-4,2000), echo=True, mp=True)
#fid = file('joint_test_2d.bpkl', 'w')
#cPickle.dump(s2, fid, protocol=2)
#fid.close()

#s_triv = S2d.spectra2d(demo_params, ns, trivial_fs, pts=[1], Npts=50,
#                       int_bounds=(1e-4,2000), echo=False, mp=True)
#fid = file('joint_test_trivial.bpkl', 'w')
#cPickle.dump(s_triv, fid, protocol=2)
#fid.close()

sel_dist1 = S1d.lognormal_dist
sel_dist2 = S2d.biv_lognorm_pdf
s1 = cPickle.load(file('joint_test_1d.bpkl'))
s2 = cPickle.load(file('joint_test_2d.bpkl'))
s1_single = cPickle.load(file('joint_test_single_pop_1d.bpkl'))
s_triv = cPickle.load(file('joint_test_trivial.bpkl'))

mu1,mu2,sigma1,sigma2,rho = 10,-8,4,4,0
ns = [20,20]
theta = 1e4

fs_rho1 = s1.integrate([mu1, sigma1], sel_dist1, theta)
fs_rho_old = s2.integrate_biv_old([mu1,mu2,sigma1,sigma2,rho],
                                  None, sel_dist2, theta, None)
fs_single_pop = s1_single.integrate([mu1, sigma1], sel_dist1, theta)

fs_rho = s2.integrate_biv([mu1,mu2,sigma1,sigma2,rho],
                          None, sel_dist2, theta, None)

#fs_triv = s_triv.integrate_biv_correct([mu1,mu2, sigma1,sigma2, rho],
#                                       None, sel_dist2, theta, None)
#fs_triv_old = s_triv.integrate_biv([mu1,mu2, sigma1,sigma2, rho],
#                                   None, sel_dist2, theta, None)

fig = plt.figure(2); fig.clear()
dadi.Plotting.plot_2d_comp_multinom(fs_rho, fs_rho1, vmin=1)
fig.axes[0].set_title('rho = 1'.format(rho))
fig.axes[1].set_title('rho = {0:.1f}'.format(rho))

gammas = s1.gammas
fig = plt.figure(21)
fig.clear()
S2d.plot_biv_dfe(gammas, gammas, sel_dist2, [mu1,mu2, sigma1, sigma2,rho])
plt.show()

fig = plt.figure(32)
fig.clear()
ax = fig.add_subplot(1,1,1)
ax.plot(fs_single_pop, '-o', label='single pop', ms=16)
ax.plot(fs_rho1.marginalize([1]), '-o', label='rho = 1', ms=12)
ax.plot(fs_rho_old.marginalize([1]), '-o', label='rho = {0:.1f}'.format(rho))
ax.plot(fs_rho.marginalize([1]), '-o',
        label='rho = {0:.1f} (corrected)'.format(rho))
ax.set_yscale('log')
ax.legend(loc='upper right')
plt.show()
