import dadi
from dadi import Numerics, PhiManip, Spectrum

import Selection as S1d
reload(S1d)

@dadi.Numerics.make_extrap_func
def equil_sel(params, ns, pts):
    gamma1 = params[0]

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx, gamma=gamma1)

    fs = Spectrum.from_phi(phi, ns, (xx,))
    return fs

demo_params, ns, pts_l = [], [10], [20,30,40]
s1 = S1d.spectra(demo_params, ns, equil_sel, pts_l=pts_l, Npts=20,
                 int_bounds=(1e-4,200), echo=False, mp=True)

s1_pos = S1d.spectra(demo_params, ns, equil_sel, pts_l=pts_l, Npts=20,
                     int_bounds=(1e-4,200), additional_gammas=[20], echo=False, mp=True)

sel_dist1 = S1d.lognormal_dist
mu,sigma,theta = 2,1,1e4

fs = s1.integrate_old([mu, sigma], sel_dist1, theta)
fs_pos = s1_pos.integrate_old([mu, sigma], sel_dist1, theta)
fs_pos_new = s1_pos.integrate([mu, sigma], sel_dist1, theta)

print('Without additional gammas: {0}'.format(fs))
print('Without additional gammas: {0}'.format(fs_pos))
print('With integrate_new: {0}'.format(fs_pos_new))
