import numpy
import numpy as np
import dadi, moments

params = [0.5,2,0.5,0.1,0,0,-10,-10]
ns = [8, 8]

# IM model with selection, dadi implementation
from dadi import Numerics, PhiManip, Integration, Spectrum
@dadi.Numerics.make_extrap_func
def fd(params, ns, pts):
    s,nu1,nu2,T,m12,m21,gamma1,gamma2 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx, gamma=gamma1)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    nu1_func = lambda t: s * (nu1/s)**(t/T)
    nu2_func = lambda t: (1-s) * (nu2/(1-s))**(t/T)
    phi = Integration.two_pops(phi, xx, T, nu1_func, nu2_func,
                               m12=m12, m21=m21, gamma1=gamma1,
                               gamma2=gamma2)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

# IM model with selection, moments implementation
def fm(params, ns, dt_fac):
    s, nu1, nu2, T, m12, m21, gamma1, gamma2 = params

    sts = moments.LinearSystem_1D.steady_state_1D(ns[0] + ns[1],
                                                 gamma=gamma1)
    fs = moments.Spectrum(sts)
    fs = moments.Manips.split_1D_to_2D(fs, ns[0], ns[1])

    nu1_func = lambda t: s * (nu1/s)**(t/T)
    nu2_func = lambda t: (1-s) * (nu2/(1-s))**(t/T)
    nu_func = lambda t: [nu1_func(t), nu2_func(t)]

    fs.integrate(nu_func, T, dt_fac=dt_fac, m=numpy.array([[0, m12], [m21, 0]]),
                gamma=np.array([gamma1, gamma2]))

    return fs

# "True" spectrum from a long dadi integration
import time
#fs_true = fd(params, ns, [400,500,600])

# Test spectra from dadi and moments
start = time.time()
fsd = fd(params, ns, [80,90,100])
print time.time() - start
fsm = fm(params, ns, 0.01)

start = time.time()
fsm_big = fm(params, [50,50], 0.01)
fsm_big_proj = fsm_big.project([8,8])
print time.time() - start

# Plot comparisons
import matplotlib.pyplot as plt
fig = plt.figure(1)
dadi.Plotting.plot_2d_comp_Poisson(fsm, fs_true, vmin=1e-6)
fig.axes[0].set_title('dadi "truth"')
fig.axes[1].set_title('moments')

fig = plt.figure(4)
dadi.Plotting.plot_2d_comp_Poisson(fsm_big_proj, fs_true, vmin=1e-6)
fig.axes[0].set_title('dadi "truth"')
fig.axes[1].set_title('moments projected')

fig = plt.figure(2)
dadi.Plotting.plot_2d_comp_Poisson(fsd, fs_true, vmin=1e-6)
fig.axes[0].set_title('dadi "truth"')
fig.axes[1].set_title('dadi short')

# Note that moments spectrum is strangely monotonic. It involves entries with
# low probability, but it's still disquieting.

plt.show()
