import numpy as np, scipy.stats.distributions as ssd
import Selection

class spectra_dom:
    def __init__(self, params, ns, func_ex, pts=None,
                 gamma_pts=100, gamma_bounds=(1e-4, 2000.), gamma_breaks=None,
                 additional_gammas=[], h_pts=21, h_bounds=[0,1],
                 mp=False, cpus=None, echo=False):
        """
        params: Optimized demographic parameters
        ns: Sample sizes
        func_ex: dadi demographic function with selection. The last two elements
                 in the parameter array should be gamma, h.
        pts: Grid point settings for func_ex
        gamma_pts: Number of gamma grid points over which to integrate
        gamma_bounds: Range of gammas to integrate over
        gamma_breaks: Break points for spacing out the gamma intervals
        additional_gammas: Sequence of additional gamma values to store results
                           for. Useful for point masses of explicit neutrality
                           or positive selection.
        h_pts: Number of h grid points over which to integrate
        h_bounds: Range of dominance coefficients to integrate over
        mp: Set to True to use multiple cores on machine
        cpus: If mp=True, this is number of cores that will be used
        echo: If True, output after each fs evaluation, to track progress.
        """
        # Store details regarding how this cache was generated. May be useful
        # for keeping track of pickled caches.
        self.ns, self.pts = ns, pts
        self.func_name, self.params = func_ex.func_name, tuple(params)

        # Create a vector of gammas that are log-spaced over sequential 
        # intervals or log-spaced over a single interval.
        if gamma_breaks is not None:
            numbreaks = len(gamma_breaks)
            stepint = gamma_pts/(numbreaks-1)
            self.gammas = []
            for i in reversed(range(0,numbreaks-1)):
                self.gammas.append(-np.logspace(np.log10(gamma_breaks[i+1]),
                                                np.log10(gamma_breaks[i]),
                                                stepint))
            self.gammas = np.concatenate(self.gammas)
        else:
            self.gammas = -np.logspace(np.log10(gamma_bounds[1]),
                                       np.log10(gamma_bounds[0]), gamma_pts)
        # Store bulk negative gammas for use in integration of negative pdf
        self.neg_gammas = self.gammas

        # Add additional gammas to array, first removing redundant ones
        additional_gammas = set(additional_gammas).difference(self.gammas)
        additional_gammas = sorted(list(additional_gammas))
        self.gammas = np.concatenate((self.gammas, additional_gammas))

        self.hs = np.linspace(h_bounds[0], h_bounds[1], h_pts)

        self.spectra = [[None]*len(self.hs) for _ in self.gammas]

        if not mp: #for running with a single thread
            for ii,gamma in enumerate(self.gammas):
                for jj,h in enumerate(self.hs):
                    self.spectra[ii][jj] = func_ex(tuple(params)+(gamma,h),
                                                   ns, pts)
                    if echo:
                        print '{0},{1}: {2},{3}'.format(ii,jj, gamma,h)
        else: #for running with with multiple cores
            import multiprocessing
            # Would like to simply use Pool.map here, but difficult to
            # pass dynamic func_ex that way.

            def worker_sfs(in_queue, outlist, popn_func_ex, params, ns, pts):
                """
                Worker function -- used to generate SFSes for
                pairs of gammas.
                """
                while True:
                    item = in_queue.get()
                    if item is None:
                        return
                    ii,jj, gamma,h = item
                    sfs = popn_func_ex(tuple(params)+(gamma,h), ns, pts)
                    print '{0},{1}: {2},{3}'.format(ii,jj, gamma,h)
                    outlist.append((ii,jj,sfs))

            manager = multiprocessing.Manager()
            if cpus is None:
                cpus = multiprocessing.cpu_count()
            work = manager.Queue(cpus-1)
            results = manager.list()

            # Assemble pool of workers
            pool = []
            for i in xrange(cpus):
                p = multiprocessing.Process(target=worker_sfs,
                                            args=(work, results, func_ex,
                                                  params, ns, pts))
                p.start()
                pool.append(p)

            # Put all jobs on queue
            for ii, gamma in enumerate(self.gammas):
                for jj, h in enumerate(self.hs):
                    work.put((ii,jj, gamma,h))
            # Put commands on queue to close out workers
            for jj in xrange(cpus):
                work.put(None)
            # Start work
            for p in pool:
                p.join()
            for ii,jj, sfs in results:
                self.spectra[ii][jj] = sfs

        # self.spectra is an array of arrays. The first two dimensions are
        # indexed by gamma,h , and the remaining dimensions
        # are the spectra themselves.
        self.spectra = np.array(self.spectra)

    def integrate_single_h(self, params, ns, sel_dist, theta, pts=None):
        """
        Integrate spectra over a prob. dist. for negative gammas and fixed h.

        params: params[:-1] are for sel_dist, and params[-1] is h
        ns: Ignored
        sel_dist: Probability distribution over gammas, taking in arguments
                  (xx, params[:-1])
        theta: Population-scaled mutation rate
        pts: Ignored

        Note that no normalization is performed, so alleles not covered by the
        specified range of gammas are assumed not to be seen in the data.

        Note also that the ns and pts arguments are ignored. They are only
        present for compatibility with other dadi functions that apply to
        demographic models.
        """
        # Pull out selection distribution parameters
        sel_params, h = params[:-1], params[-1]

        # Find the indices above and below the given value of h. We will
        # integrate over gamma for these values of h, then linearly interpolate
        # between them. Note that if requested h is exactly in our self.hs
        # array, then we'll be doing twice as much work as necessary. Maybe
        # we'll special-case that at some point.
        h_ii_up = min(np.searchsorted(self.hs, h, side='right'), len(self.hs)-1)
        h_ii_down = h_ii_up-1

        # Restrict our gammas and spectra to negative gammas.
        Nneg = len(self.neg_gammas)
        spectra_up = self.spectra[:Nneg, h_ii_up]
        spectra_down = self.spectra[:Nneg, h_ii_down]

        # Weights for integration
        weights = sel_dist(-self.neg_gammas, sel_params)

        # Apply weighting. Could probably do this in a single fancy numpy
        # multiplication, which might be faster.
        weighted_spectra_up = 0*spectra_up
        weighted_spectra_down = 0*spectra_down
        for ii, w in enumerate(weights):
            weighted_spectra_up[ii] = w*spectra_up[ii]
            weighted_spectra_down[ii] = w*spectra_down[ii]

        # Integrate out the first axis, which is the gamma axis.
        fs_up = -np.trapz(weighted_spectra_up, -self.neg_gammas, axis=0)
        fs_down = -np.trapz(weighted_spectra_down, -self.neg_gammas, axis=0)

        # Linear interpolation
        p = (self.hs[h_ii_up] - h)/(self.hs[h_ii_up] - self.hs[h_ii_down])
        fs = fs_up * (1-p) + fs_down * p

        return Spectrum(theta*fs)

# Probability distribution functions
def lognorm_pdf(xx, (mu, sigma)):
    """
    Lognormal probability distribution function

    xx: x-coordinates at which to evaluate.
    params: Input parameters (mu, sigma).
    """
    return ssd.lognorm.pdf(xx, sigma, scale=np.exp(mu))

#
# Example demography + selection functions
#
import dadi
from dadi import Numerics, PhiManip, Integration
from dadi.Spectrum_mod import Spectrum

def IM_sel_h(params, ns, pts):
    """
    General IM model
    """
    s,nu1,nu2,T,m12,m21,gamma1,gamma2,h1,h2 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx, gamma=gamma1, h=h1)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    nu1_func = lambda t: s * (nu1/s)**(t/T)
    nu2_func = lambda t: (1-s) * (nu2/(1-s))**(t/T)
    phi = Integration.two_pops(phi, xx, T, nu1_func, nu2_func,
                               m12=m12, m21=m21, gamma1=gamma1,
                               gamma2=gamma2, h1=h1, h2=h2)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def IM_single_sel_h(params, ns, pts):
    """
    IM model for single s and h.
    """
    s,nu1,nu2,T,m12,m21,gamma,h = params
    return IM_sel_h([s,nu1,nu2,T,m12,m21,gamma,gamma,h,h], ns, pts)

def IM_single_sel_genic(params, ns, pts):
    """
    IM model for h=0.5.
    """
    s,nu1,nu2,T,m12,m21,gamma = params
    return IM_single_sel_h([s,nu1,nu2,T,m12,m21,gamma,0.5], ns, pts)


def IM_pre_sel_h(params, ns, pts):
    """
    General IM pre model
    """
    nuPre,TPre,s,nu1,nu2,T,m12,m21,gamma1,gamma2,h1,h2 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx, gamma=gamma1, h=h1)
    phi = Integration.one_pop(phi, xx, TPre, nu=nuPre, gamma=gamma1, h=h1)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    nu1_0 = nuPre*s
    nu2_0 = nuPre*(1-s)
    nu1_func = lambda t: nu1_0 * (nu1/nu1_0)**(t/T)
    nu2_func = lambda t: nu2_0 * (nu2/nu2_0)**(t/T)
    phi = Integration.two_pops(phi, xx, T, nu1_func, nu2_func,
                               m12=m12, m21=m21, gamma1=gamma1,
                               gamma2=gamma2, h1=h1, h2=h2)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def IM_pre_single_sel_h(params, ns, pts):
    """
    IM pre model for single gamma and h.
    """
    nuPre,TPre,s,nu1,nu2,T,m12,m21,gamma,h = params
    return IM_pre_sel_h([nuPre,TPre,s,nu1,nu2,T,m12,m21,gamma,gamma,h,h], ns, pts)



if __name__ == '__main__':
    #
    # Quick check. Ensure we get identical results for h = 0.5 to previous
    # 1D integration code.
    #
    demo_params = [0.95, 3.5, 0.2, 0.08, 0.8, 3]
    ns, pts_l = [5, 7], [20,30,40]
    gamma_pts, gamma_bounds = 7, (1e-4, 20)
    h_pts = 5
    theta = 1e4
    sel_params = [5.4, 3.5, 0.5]

    func_ex = Numerics.make_extrap_func(IM_single_sel_h)
    s = spectra_dom(demo_params, ns, func_ex, pts=pts_l,
                    gamma_pts=gamma_pts, gamma_bounds=gamma_bounds,
                    h_pts=h_pts, echo=True, mp=True)
    sel_dist = lognorm_pdf
    fs = s.integrate_single_h(sel_params, None, sel_dist, theta=theta)

    genic_func_ex = Numerics.make_extrap_func(IM_single_sel_genic)
    s_genic = Selection.spectra(demo_params, ns, genic_func_ex, pts_l=pts_l,
                                Npts=gamma_pts, int_bounds=gamma_bounds,
                                echo=True, mp=True)
    fs_genic = s_genic.integrate(sel_params[:-1], Selection.lognormal_dist,
                                 theta=theta)
    assert(np.allclose(fs, fs_genic))
